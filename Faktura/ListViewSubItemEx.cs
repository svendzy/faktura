﻿using System;
using System.Windows.Forms;

namespace Faktura
{
    public class ListViewSubItemEx : ListViewItem.ListViewSubItem
    {
        private HorizontalAlignment textAlign;

        public ListViewSubItemEx() 
            : base()
        {
            this.textAlign = HorizontalAlignment.Left;
        }

        public ListViewSubItemEx(ListViewItem owner, string text, HorizontalAlignment textAlign)
            : base(owner, text)
        {
            this.textAlign = textAlign;
        }

        public ListViewSubItemEx(ListViewItem owner, string text, System.Drawing.Color foreColor, System.Drawing.Color backColor, System.Drawing.Font font)
            : base(owner, text, foreColor, backColor, font)
        {
            this.textAlign = HorizontalAlignment.Left;
        }

        public HorizontalAlignment TextAlign
        {
            get
            {
                return textAlign;
            }
            set
            {
                textAlign = value;
            }
        }
    }
}
