﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using Faktura.Presentation;
using Faktura.Common;
using Faktura.Config;

namespace Faktura
{
    public partial class V_FakturaListe : UserControl, IV_ToolStrip, IV_FakturaListe, IPrintable, IReportStatus
    {
        private ComboBox cbxFilterAar;
        private ComboBox cbxFilterKunde;
        private RadioButton rbFakturaStatusAlle;
        private RadioButton rbFakturaStatusBehandler;
        private RadioButton rbFakturaStatusSendt;
        private RadioButton rbFakturaStatusInnbetalt;
        public event ReportStatusEventHandler ReportStatus;

        private IList<Common.KundeNavn> kundeNavn;
        private P_FakturaListe p_FakturaListe;
        private IList<Common.FakturaPreview> fakturaListe;
        private FakturaFilter fakturaFilter;
        private int[] fakturaAar;
        private int valgtFakturaId;
        private int valgtIndeks;

        public V_FakturaListe()
        {
            InitializeComponent();

            lwFakturaListe.ColumnWeights = new float[] { 0.1f, 0.24f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.08f, 0.08f };

            p_FakturaListe = new P_FakturaListe(this);
            p_FakturaListe.Init();

            InitFilterToolstrip();
        }

        public IList<KundeNavn> KundeNavn
        {
            set
            {
                kundeNavn = value;
            }
        }

        public IList<Common.FakturaPreview> FakturaListe
        {
            set
            {
                if (fakturaListe != null)
                    fakturaListe = null;
                fakturaListe = value;
            }
        }

        public FakturaFilter FakturaFilter
        {
            get
            {
                return fakturaFilter;
            }
            set
            {
                fakturaFilter = value;
            }
        }

        public int[] FakturaAar
        {
            get
            {
                return fakturaAar;
            }
            set
            {
                fakturaAar = value;
            }
        }

        public int ValgtFakturaId
        {
            get
            {
                return valgtFakturaId;
            }
        }

        private void InitFilterToolstrip()
        {
            Label lblFakturaAar = new Label();
            lblFakturaAar.Text = "Velg år:";
            lblFakturaAar.TextAlign = ContentAlignment.MiddleCenter;
            lblFakturaAar.Padding = new Padding(6);
            ToolStripControlHost _lblFakturaAar = new ToolStripControlHost(lblFakturaAar);
            cbxFilterAar = new ComboBox();
            cbxFilterAar.Width = 80;
            cbxFilterAar.DropDownStyle = ComboBoxStyle.DropDown;
            cbxFilterAar.Padding = new Padding(6);
            cbxFilterAar.DropDown += new EventHandler(cbxFilterAar_DropDown);
            cbxFilterAar.SelectedIndexChanged += new EventHandler(cbxFilterAar_SelectedIndexChanged);
            cbxFilterAar.KeyPress += new KeyPressEventHandler(cbxFilterAar_KeyPress);
            ToolStripControlHost _cbxFilterAar = new ToolStripControlHost(cbxFilterAar);

            Label lblFilterKunde = new Label();
            lblFilterKunde.Text = "Velg kunde:";
            lblFilterKunde.TextAlign = ContentAlignment.MiddleCenter;
            lblFilterKunde.Padding = new Padding(6);
            ToolStripControlHost _lblFilterKunde = new ToolStripControlHost(lblFilterKunde);
            cbxFilterKunde = new ComboBox();
            cbxFilterKunde.Width = 150;
            cbxFilterKunde.DropDownStyle = ComboBoxStyle.DropDown;
            cbxFilterKunde.Padding = new Padding(6);
            cbxFilterKunde.DropDown += new EventHandler(cbxFilterKunde_DropDown);
            cbxFilterKunde.SelectedIndexChanged += new EventHandler(cbxFilterKunde_SelectedIndexChanged);
            cbxFilterKunde.KeyPress += new KeyPressEventHandler(cbxFilterKunde_KeyPress);
            cbxFilterKunde.TabStop = false;
            ToolStripControlHost _cbxFilterKunde = new ToolStripControlHost(cbxFilterKunde);

            Label lblFakturaStatus = new Label();
            lblFakturaStatus.Text = "Velg status:";
            lblFakturaStatus.TextAlign = ContentAlignment.MiddleCenter;
            lblFakturaStatus.Padding = new Padding(6);
            ToolStripControlHost _lblFakturaStatus = new ToolStripControlHost(lblFakturaStatus);
            rbFakturaStatusAlle = new RadioButton();
            rbFakturaStatusAlle.Text = "Alle";
            rbFakturaStatusAlle.Checked = true;
            rbFakturaStatusAlle.Padding = new Padding(6);
            rbFakturaStatusAlle.CheckedChanged += new EventHandler(rbFakturaStatusAlle_CheckedChanged);
            ToolStripControlHost _rbFakturaStatusAlle = new ToolStripControlHost(rbFakturaStatusAlle);
            rbFakturaStatusBehandler = new RadioButton();
            rbFakturaStatusBehandler.Text = "Behandler";
            rbFakturaStatusBehandler.Checked = false;
            rbFakturaStatusBehandler.Padding = new Padding(6);
            rbFakturaStatusBehandler.CheckedChanged += new EventHandler(rbFakturaStatusBehandler_CheckedChanged);
            ToolStripControlHost _rbFakturaStatusBehandler = new ToolStripControlHost(rbFakturaStatusBehandler);
            rbFakturaStatusSendt = new RadioButton();
            rbFakturaStatusSendt.Text = "Sendt";
            rbFakturaStatusSendt.Checked = false;
            rbFakturaStatusSendt.Padding = new Padding(6);
            rbFakturaStatusSendt.CheckedChanged += new EventHandler(rbFakturaStatusSendt_CheckedChanged);
            ToolStripControlHost _rbFakturaStatusSendt = new ToolStripControlHost(rbFakturaStatusSendt);
            rbFakturaStatusInnbetalt = new RadioButton();
            rbFakturaStatusInnbetalt.Text = "Innbetalt";
            rbFakturaStatusInnbetalt.Checked = false;
            rbFakturaStatusInnbetalt.Padding = new Padding(6);
            rbFakturaStatusInnbetalt.CheckedChanged += new EventHandler(rbFakturaStatusInnbetalt_CheckedChanged);
            ToolStripControlHost _rbFakturaStatusInnbetalt = new ToolStripControlHost(rbFakturaStatusInnbetalt);
            tsFilter.Items.Add(_lblFakturaAar);
            tsFilter.Items.Add(_cbxFilterAar);
            tsFilter.Items.Add(_lblFilterKunde);
            tsFilter.Items.Add(_cbxFilterKunde);
            tsFilter.Items.Add(_lblFakturaStatus);
            tsFilter.Items.Add(_rbFakturaStatusAlle);
            tsFilter.Items.Add(_rbFakturaStatusBehandler);
            tsFilter.Items.Add(_rbFakturaStatusSendt);
            tsFilter.Items.Add(_rbFakturaStatusInnbetalt);
        }

        private void cbxFilterAar_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cbxFilterKunde_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cbxFilterKunde_DropDown(object sender, EventArgs e)
        {
            p_FakturaListe.HentKundeNavn();
            cbxFilterKunde.BeginUpdate();
            cbxFilterKunde.Items.Clear();
            cbxFilterKunde.Items.Add("Alle");
            for (int nKunde = 0; nKunde < kundeNavn.Count; nKunde++)
            {
                cbxFilterKunde.Items.Add(kundeNavn[nKunde].Navn);
            }
            cbxFilterKunde.EndUpdate();
        }

        private void cbxFilterKunde_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxFilterKunde.SelectedIndex != -1)
            {
                if (cbxFilterKunde.SelectedIndex == 0)
                    fakturaFilter.KundeId = -1;
                else
                    fakturaFilter.KundeId = kundeNavn[cbxFilterKunde.SelectedIndex - 1].Id;
                hentFakturaListeTask_Run();
                lwFakturaListe.Select();
            }
        }

        private void cbxFilterAar_DropDown(object sender, EventArgs e)
        {
            p_FakturaListe.HentFakturaAar();
            cbxFilterAar.BeginUpdate();
            cbxFilterAar.Items.Clear();
            for (int nAar = 0; nAar < fakturaAar.Length; nAar++)
            {
                cbxFilterAar.Items.Add(fakturaAar[nAar]);
            }
            cbxFilterAar.EndUpdate();
        }

        private void cbxFilterAar_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxFilterAar.SelectedIndex != -1)
            {
                fakturaFilter.Aar = fakturaAar[cbxFilterAar.SelectedIndex];
                hentFakturaListeTask_Run();
            }
        }

        private void rbFakturaStatusInnbetalt_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFakturaStatusInnbetalt.Checked)
            {
                fakturaFilter.Status = FakturaFilterStatus.Innbetalt;
                hentFakturaListeTask_Run();
            }
        }

        private void rbFakturaStatusSendt_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFakturaStatusSendt.Checked)
            {
                fakturaFilter.Status = FakturaFilterStatus.Sendt;
                hentFakturaListeTask_Run();
            }
        }

        private void rbFakturaStatusBehandler_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFakturaStatusBehandler.Checked)
            {
                fakturaFilter.Status = FakturaFilterStatus.Behandler;
                hentFakturaListeTask_Run();
            }
        }

        private void rbFakturaStatusAlle_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFakturaStatusAlle.Checked)
            {
                fakturaFilter.Status = FakturaFilterStatus.Alle;
                hentFakturaListeTask_Run();
            }
        }

        public ToolStrip ToolStrip
        {
            get
            {
                return tsFakturaListe;
            }
        }

        private void DrawListViewItems()
        {
            lwFakturaListe.BeginUpdate();
            lwFakturaListe.Items.Clear();
            if ((fakturaListe != null) && (fakturaListe.Count > 0))
            {
                Color farge_BehandlerBakgrunn = I_Konfig.UI.BehandlerBakgrunn;
                Color farge_BehandlerTekst = I_Konfig.UI.BehandlerForgrunn;
                Color farge_SendtBakgrunn = I_Konfig.UI.SendtBakgrunn;
                Color farge_SendtTekst = I_Konfig.UI.SendtForgrunn;
                Color farge_KreditertBakgrunn = I_Konfig.UI.KreditertBakgrunn;
                Color farge_KreditertTekst = I_Konfig.UI.KreditertForgrunn;
                Color farge_InnbetaltBakgrunn = I_Konfig.UI.InnbetaltBakgrunn;
                Color farge_InnbetaltTekst = I_Konfig.UI.InnbetaltForgrunn;

                for (int n = 0; n < fakturaListe.Count; n++)
                {
                    ListViewItemEx item = new ListViewItemEx();
                    item.TextAlign = HorizontalAlignment.Center;
                    item.Tag = fakturaListe[n];
                    if (fakturaListe[n].Kredittnota)
                    {
                        item.BackColor = farge_KreditertBakgrunn;
                        item.ForeColor = farge_KreditertTekst;
                    }
                    else
                    {
                        switch (fakturaListe[n].Status)
                        {
                            case FakturaStatus.KlarForSending:
                                item.BackColor = farge_BehandlerBakgrunn;
                                item.ForeColor = farge_BehandlerTekst;
                                break;
                            case FakturaStatus.Sendt:
                                item.BackColor = farge_SendtBakgrunn;
                                item.ForeColor = farge_SendtTekst;
                                break;
                            case FakturaStatus.Innbetalt:
                                item.BackColor = farge_InnbetaltBakgrunn;
                                item.ForeColor = farge_InnbetaltTekst;
                                break;
                        }
                    }
                    item.Text = Convert.ToString((fakturaListe[n].Nr != -1) ? Convert.ToString(fakturaListe[n].Nr) : "Ikke satt");
                    item.SubItems.Add(new ListViewSubItemEx(item, fakturaListe[n].KundeNavn, HorizontalAlignment.Left));
                    item.SubItems.Add(new ListViewSubItemEx(item, (fakturaListe[n].Dato != null) ? 
                        ((DateTime)fakturaListe[n].Dato).ToShortDateString() : "Ikke satt", HorizontalAlignment.Center));
                    item.SubItems.Add(new ListViewSubItemEx(item, fakturaListe[n].KalkulertBeløp.ToString("F"), HorizontalAlignment.Right));
                    item.SubItems.Add(new ListViewSubItemEx(item, fakturaListe[n].KalkulertMva.ToString("F"), HorizontalAlignment.Right));
                    item.SubItems.Add(new ListViewSubItemEx(item, fakturaListe[n].KalkulertSum.ToString("F"), HorizontalAlignment.Right));
                    item.SubItems.Add(new ListViewSubItemEx(item, Convert.ToString(fakturaListe[n].Status), HorizontalAlignment.Center));
                    item.SubItems.Add(new ListViewSubItemEx(item, (fakturaListe[n].Kreditert) ? "Ja" : String.Empty, HorizontalAlignment.Center));
                    item.SubItems.Add(new ListViewSubItemEx(item, (fakturaListe[n].Kredittnota) ? "Ja" : String.Empty, HorizontalAlignment.Center));
                    lwFakturaListe.Items.Add(item);
                }
            }
            lwFakturaListe.EndUpdate();
        }

        private void V_FakturaListe_Load(object sender, EventArgs e)
        {
            lwFakturaListe.ReCalculateColumnWidths();
            cbxFilterAar.Text = Convert.ToString(DateTime.Now.Year);
            cbxFilterKunde.Text = "Alle";
            
            hentFakturaListeTask_Run();
        }

        private void SetToolstripState(Common.FakturaPreview faktura)
        {
          /*  switch (faktura.Status)
            {
                case FakturaStatus.Behandler:
                    tsBtnRedigerFaktura.Enabled = true;
                    tsBtnSendPrisoverslag.Enabled = true;
                    tsBtnSendFaktura.Enabled = true;
                    tsBtnKrediterFaktura.Enabled = false;
                    tsBtnRegistrerInnbetaling.Enabled = false;
                    tsBtnSlettFaktura.Enabled = true;
                    break;
                case FakturaStatus.Sendt:
                    tsBtnRedigerFaktura.Enabled = false;
                    tsBtnSendPrisoverslag.Enabled = false;
                    tsBtnSendFaktura.Enabled = true;
                    tsBtnKrediterFaktura.Enabled = true;
                    tsBtnRegistrerInnbetaling.Enabled = true;
                    tsBtnSlettFaktura.Enabled = false;
                    break;
                case FakturaStatus.Innbetalt:
                    tsBtnRedigerFaktura.Enabled = false;
                    tsBtnSendPrisoverslag.Enabled = false;
                    tsBtnSendFaktura.Enabled = true;
                    tsBtnKrediterFaktura.Enabled = (faktura.Kredittnota || faktura.Kreditert) ? false : true;
                    tsBtnRegistrerInnbetaling.Enabled = false;
                    tsBtnSlettFaktura.Enabled = false;
                    break;
            }*/
        }

        private void lwFakturaListe_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((lwFakturaListe.SelectedIndices != null) && (lwFakturaListe.SelectedIndices.Count == 1))
            {
                FakturaPreview valgtFaktura = fakturaListe[lwFakturaListe.SelectedIndices[0]];
                valgtFakturaId = valgtFaktura.Id;
                valgtIndeks = lwFakturaListe.SelectedIndices[0];
                SetToolstripState(valgtFaktura);
            }
        }

        private void V_FakturaListe_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                lwFakturaListe.Select();
            }
        }

        private void tsBtnFilter_CheckedChanged(object sender, EventArgs e)
        {
            tsFilter.Visible = tsBtnFilter.Checked;
        }

        private void tsBtnSendFaktura_Click(object sender, EventArgs e)
        {
            if (valgtFakturaId > 0)
            {
                V_Bekreft v_Bekreft = new V_Bekreft();
                v_Bekreft.Text = "Send faktura";
                DialogResult r = v_Bekreft.ShowDialog();
                if (r == DialogResult.OK)
                {
                    sendFakturaTask_Run();
                }
            }
        }

        private void tsBtnKrediterFaktura_Click(object sender, EventArgs e)
        {
            if (valgtFakturaId > 0)
            {
                V_Bekreft v_Bekreft = new V_Bekreft();
                v_Bekreft.Text = "Krediter faktura";
                DialogResult r = v_Bekreft.ShowDialog();
                if (r == DialogResult.OK)
                {
                    p_FakturaListe.KrediterFaktura();
                    hentFakturaListeTask_Run();
                }
            }
        }

        private void tsBtnRegistrerInnbetaling_Click(object sender, EventArgs e)
        {
            if (valgtFakturaId > 0)
            {
                V_Bekreft v_Bekreft = new V_Bekreft();
                v_Bekreft.Text = "Registrer innbetaling";
                DialogResult r = v_Bekreft.ShowDialog();
                if (r == DialogResult.OK)
                {
                    p_FakturaListe.RegistrerInnbetaling();
                    hentFakturaListeTask_Run();
                }
            }
        }

        public override void Refresh()
        {
            DrawListViewItems();
        }

        private void slettFakturaBackgroundTask_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            p_FakturaListe.SlettFaktura();
        }

        private void slettFakturaBackgroundTask_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                slettFakturaTask_End();
            }
            else
            {
                slettFakturaTask_Error();
            }
        }

        private void hentFakturaListeBackgroundTask_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            p_FakturaListe.HentFakturaListe();
        }

        private void hentFakturaListeBackgroundTask_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                DrawListViewItems();
                hentFakturaListeTask_End();
            }
            else
            {
                hentFakturaListeTask_Error();
            }
        }

        private void sendFakturaBackgroundTask_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            p_FakturaListe.SendFaktura();
        }

        private void sendFakturaBackgroundTask_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                sendFakturaTask_End();
                hentFakturaListeTask_Run();
            }
            else
            {
                sendFakturaTask_Error();
            }
        }

        private void sendPrisoverslagBackgroundTask_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            p_FakturaListe.SendPrisoverslag();
        }

        private void sendPrisoverslagBackgroundTask_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                sendPrisoverslagTask_End();
            }
            else
            {
                sendPrisoverslagTask_Error();
            }
        }

        private void tsBtnVisFaktura_Click(object sender, EventArgs e)
        {
            p_FakturaListe.VisFaktura();
        }

        private void hentFakturaListeTask_Run()
        {
            if (!hentFakturaListeBackgroundTask.IsBusy)
            {
                OnReportStatus(new ReportStatusEventArgs("Henter fakturaliste..."));
                hentFakturaListeBackgroundTask.RunWorkerAsync();
            }
        }

        private void hentFakturaListeTask_End()
        {
            OnReportStatus(new ReportStatusEventArgs("Ferdig"));
        }

        private void hentFakturaListeTask_Error()
        {
            OnReportStatus(new ReportStatusEventArgs("Feil ved henting av fakturaliste", true));
        }

        private void sendFakturaTask_Run()
        {
            if (!sendFakturaBackgroundTask.IsBusy)
            {
                OnReportStatus(new ReportStatusEventArgs("Sender faktura..."));
                sendFakturaBackgroundTask.RunWorkerAsync();
            }
        }

        private void sendFakturaTask_End()
        {
            OnReportStatus(new ReportStatusEventArgs("Ferdig"));
        }

        private void sendFakturaTask_Error()
        {
            OnReportStatus(new ReportStatusEventArgs("Feil ved sending av faktura", true));
        }


        private void slettFakturaTask_Run()
        {
            if (!slettFakturaBackgroundTask.IsBusy)
            {
                OnReportStatus(new ReportStatusEventArgs("Sletter faktura..."));
                slettFakturaBackgroundTask.RunWorkerAsync();
            }
        }

        private void slettFakturaTask_End()
        {
            OnReportStatus(new ReportStatusEventArgs("Ferdig"));
        }

        private void slettFakturaTask_Error()
        {
            OnReportStatus(new ReportStatusEventArgs("Feil ved sletting av faktura", true));
        }


        private void sendPrisoverslagTask_Run()
        {
            if (!sendPrisoverslagBackgroundTask.IsBusy)
            {
                OnReportStatus(new ReportStatusEventArgs("Sender prisoverslag..."));
                sendPrisoverslagBackgroundTask.RunWorkerAsync();
            }
        }

        private void sendPrisoverslagTask_End()
        {
            OnReportStatus(new ReportStatusEventArgs("Ferdig"));
        }

        private void sendPrisoverslagTask_Error()
        {
            OnReportStatus(new ReportStatusEventArgs("Feil ved sending av prisoverslag", true));
        }

        void IPrintable.Print()
        {
            throw new NotImplementedException();
        }

        protected void OnReportStatus(ReportStatusEventArgs e)
        {
            if (ReportStatus != null)
            {
                ReportStatus(this, e);
            }
        }

        void IV_Oppdaterbar.Oppdater(bool bLast)
        {
            if (bLast)
                hentFakturaListeTask_Run();
            else
                DrawListViewItems();
        }
    }
}
