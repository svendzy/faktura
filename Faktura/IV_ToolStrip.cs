﻿using System;
using System.Windows.Forms;

namespace Faktura
{
    public interface IV_ToolStrip
    {
        ToolStrip ToolStrip { get; }
    }
}
