﻿using System;
using System.Windows.Forms;

using Faktura.Presentation;
using Faktura.Common;

namespace Faktura
{
    public partial class V_Kunde : Form, IV_Kunde
    {
        private P_Kunde p_Kunde;
        private Kunde kunde;

        public V_Kunde()
        {
            InitializeComponent();
            this.p_Kunde = new P_Kunde(this);
            this.p_Kunde.Init();
        }

        public Kunde Kunde
        {
            get
            {
                return kunde;
            }
            set
            {
                kunde = value;
            }
        }

        private void V_Kunde_Load(object sender, EventArgs e)
        {
            if (kunde.Id != -1)
            {
                this.Text = "Rediger kunde";
                txtKundeId.Text = Convert.ToString(kunde.Id);
            }
            else
            {
                this.Text = "Ny kunde";
                lblKundeId.Text = "Automatisk";
            }
            txtNavn.Text = kunde.Navn;
            txtPostadresse.Text = kunde.Postadresse;
            txtPostnr.Text = kunde.PostNr;
            txtPoststed.Text = kunde.PostSted;
            txtFakturaEpost.Text = kunde.FakturaEpost;
            txtGrunnlagEpost.Text = kunde.GrunnlagEpost;

            switch (kunde.KundeType)
            {
                case KundeType.Privat:
                    rbPrivatKunde.Checked = true;
                    break;
                case KundeType.Bedrift:
                    rbBedriftskunde.Checked = true;
                    break;
            }

            switch (kunde.FakturaMetode)
            {
                case FakturaMetode.Epost:
                    rbFakturaMetodeEpost.Checked = true;
                    break;
                case FakturaMetode.Papir:
                    rbFakturaMetodePapir.Checked = true;
                    break;
            }
        }

        private void btnLagre_Click(object sender, EventArgs e)
        {
            kunde.Navn = txtNavn.Text;
            kunde.Postadresse = txtPostadresse.Text;
            kunde.PostNr = txtPostnr.Text;
            kunde.PostSted = txtPoststed.Text;
            kunde.FakturaEpost = txtFakturaEpost.Text;
            kunde.GrunnlagEpost = txtGrunnlagEpost.Text; 
            kunde.KundeType = (rbPrivatKunde.Checked) ? KundeType.Privat : KundeType.Bedrift;
            kunde.FakturaMetode = (rbFakturaMetodeEpost.Checked) ? FakturaMetode.Epost : FakturaMetode.Papir;
            if (!lagreBackgroundTask.IsBusy)
            {
                lagreBackgroundTask.RunWorkerAsync();
            }
        }

        private void lagreBackgroundTask_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            p_Kunde.LagreKunde();
        }

        private void lagreBackgroundTask_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
