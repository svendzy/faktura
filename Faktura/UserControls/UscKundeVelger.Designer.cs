﻿namespace Faktura.UserControls
{
    partial class UscKundeVelger
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKunde = new System.Windows.Forms.Label();
            this.cbxMottaker = new System.Windows.Forms.ComboBox();
            this.lblPostAdresse = new System.Windows.Forms.Label();
            this.txtPostAdresse = new System.Windows.Forms.TextBox();
            this.lblPoststed = new System.Windows.Forms.Label();
            this.txtPostNr = new System.Windows.Forms.TextBox();
            this.lblSendTilEpost = new System.Windows.Forms.Label();
            this.txtSendTilEpost = new System.Windows.Forms.TextBox();
            this.txtPoststed = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblKunde
            // 
            this.lblKunde.AutoSize = true;
            this.lblKunde.Location = new System.Drawing.Point(3, 9);
            this.lblKunde.Name = "lblKunde";
            this.lblKunde.Size = new System.Drawing.Size(41, 13);
            this.lblKunde.TabIndex = 0;
            this.lblKunde.Text = "Kunde:";
            // 
            // cbxMottaker
            // 
            this.cbxMottaker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxMottaker.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbxMottaker.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbxMottaker.FormattingEnabled = true;
            this.cbxMottaker.Location = new System.Drawing.Point(84, 6);
            this.cbxMottaker.Name = "cbxMottaker";
            this.cbxMottaker.Size = new System.Drawing.Size(212, 21);
            this.cbxMottaker.TabIndex = 0;
            this.cbxMottaker.SelectedIndexChanged += new System.EventHandler(this.cbxMottaker_SelectedIndexChanged);
            // 
            // lblPostAdresse
            // 
            this.lblPostAdresse.AutoSize = true;
            this.lblPostAdresse.Location = new System.Drawing.Point(3, 36);
            this.lblPostAdresse.Name = "lblPostAdresse";
            this.lblPostAdresse.Size = new System.Drawing.Size(68, 13);
            this.lblPostAdresse.TabIndex = 0;
            this.lblPostAdresse.Text = "Postadresse:";
            // 
            // txtPostAdresse
            // 
            this.txtPostAdresse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPostAdresse.BackColor = System.Drawing.Color.White;
            this.txtPostAdresse.Location = new System.Drawing.Point(84, 33);
            this.txtPostAdresse.Name = "txtPostAdresse";
            this.txtPostAdresse.ReadOnly = true;
            this.txtPostAdresse.Size = new System.Drawing.Size(212, 20);
            this.txtPostAdresse.TabIndex = 1;
            this.txtPostAdresse.TabStop = false;
            // 
            // lblPoststed
            // 
            this.lblPoststed.AutoSize = true;
            this.lblPoststed.Location = new System.Drawing.Point(3, 62);
            this.lblPoststed.Name = "lblPoststed";
            this.lblPoststed.Size = new System.Drawing.Size(51, 13);
            this.lblPoststed.TabIndex = 0;
            this.lblPoststed.Text = "Poststed:";
            // 
            // txtPostNr
            // 
            this.txtPostNr.BackColor = System.Drawing.Color.White;
            this.txtPostNr.Location = new System.Drawing.Point(84, 59);
            this.txtPostNr.Name = "txtPostNr";
            this.txtPostNr.ReadOnly = true;
            this.txtPostNr.Size = new System.Drawing.Size(36, 20);
            this.txtPostNr.TabIndex = 2;
            this.txtPostNr.TabStop = false;
            // 
            // lblSendTilEpost
            // 
            this.lblSendTilEpost.AutoSize = true;
            this.lblSendTilEpost.Location = new System.Drawing.Point(4, 88);
            this.lblSendTilEpost.Name = "lblSendTilEpost";
            this.lblSendTilEpost.Size = new System.Drawing.Size(74, 13);
            this.lblSendTilEpost.TabIndex = 0;
            this.lblSendTilEpost.Text = "Send til epost:";
            // 
            // txtSendTilEpost
            // 
            this.txtSendTilEpost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSendTilEpost.BackColor = System.Drawing.Color.White;
            this.txtSendTilEpost.Location = new System.Drawing.Point(84, 85);
            this.txtSendTilEpost.Name = "txtSendTilEpost";
            this.txtSendTilEpost.ReadOnly = true;
            this.txtSendTilEpost.Size = new System.Drawing.Size(212, 20);
            this.txtSendTilEpost.TabIndex = 4;
            this.txtSendTilEpost.TabStop = false;
            // 
            // txtPoststed
            // 
            this.txtPoststed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPoststed.BackColor = System.Drawing.Color.White;
            this.txtPoststed.Location = new System.Drawing.Point(126, 59);
            this.txtPoststed.Name = "txtPoststed";
            this.txtPoststed.ReadOnly = true;
            this.txtPoststed.Size = new System.Drawing.Size(170, 20);
            this.txtPoststed.TabIndex = 3;
            this.txtPoststed.TabStop = false;
            // 
            // UscKundeVelger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.txtPoststed);
            this.Controls.Add(this.txtSendTilEpost);
            this.Controls.Add(this.lblSendTilEpost);
            this.Controls.Add(this.txtPostNr);
            this.Controls.Add(this.lblPoststed);
            this.Controls.Add(this.txtPostAdresse);
            this.Controls.Add(this.lblPostAdresse);
            this.Controls.Add(this.lblKunde);
            this.Controls.Add(this.cbxMottaker);
            this.DoubleBuffered = true;
            this.Name = "UscKundeVelger";
            this.Size = new System.Drawing.Size(311, 117);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKunde;
        private System.Windows.Forms.ComboBox cbxMottaker;
        private System.Windows.Forms.Label lblPostAdresse;
        private System.Windows.Forms.TextBox txtPostAdresse;
        private System.Windows.Forms.Label lblPoststed;
        private System.Windows.Forms.TextBox txtPostNr;
        private System.Windows.Forms.Label lblSendTilEpost;
        private System.Windows.Forms.TextBox txtSendTilEpost;
        private System.Windows.Forms.TextBox txtPoststed;
    }
}
