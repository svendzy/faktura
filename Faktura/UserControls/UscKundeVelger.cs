﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Faktura.Presentation;

namespace Faktura.UserControls
{
    public partial class UscKundeVelger : UserControl, IV_KundeVelger
    {
        private Common.Kunde kunde;
        private IList<Common.Kunde> kundeListe;
        public event System.EventHandler KundeValgt;

        public UscKundeVelger()
        {
            InitializeComponent();
        }

        public IList<Common.Kunde> KundeListe
        {
            set
            {
                kundeListe = value;
                for (int n = 0; n < kundeListe.Count; n++)
                {
                    cbxMottaker.Items.Add(kundeListe[n].Navn);
                }
            }
        }

        public Common.Kunde Kunde
        {
            get
            {
                return kunde;
            }
            set
            {
                kunde = value;
                if (kunde != null)
                {
                    cbxMottaker.Text = kunde.Navn;
                    txtPostAdresse.Text = kunde.Postadresse;
                    txtPostNr.Text = kunde.PostNr;
                    txtPoststed.Text = kunde.PostSted;
                    txtSendTilEpost.Text = kunde.GrunnlagEpost;
                }
            }
        }

        protected void OnKundeValgt()
        {
            if (KundeValgt != null)
            {
                KundeValgt(this, new EventArgs());
            }
        }

        private void cbxMottaker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((kundeListe != null) && (cbxMottaker.SelectedIndex != -1))
            {
                kunde = kundeListe[cbxMottaker.SelectedIndex];
                txtPostAdresse.Text = kunde.Postadresse;
                txtPostNr.Text = kunde.PostNr;
                txtPoststed.Text = kunde.PostSted;
                txtSendTilEpost.Text = kunde.GrunnlagEpost;
                OnKundeValgt();
            }
        }
    }
}
