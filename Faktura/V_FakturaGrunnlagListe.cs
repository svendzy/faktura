﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

using Faktura.Presentation;
using Faktura.Common;
using Faktura.Config;

namespace Faktura
{
    public partial class V_FakturaGrunnlagListe : UserControl, IV_FakturaGrunnlagListe, IV_ToolStrip, IPrintable, IReportStatus
    {
        public event ReportStatusEventHandler ReportStatus;
        private P_FakturaGrunnlagListe p_FakturagrunnlagListe;
        private int[] aar;
        private IList<Kunde> kundeListe;
        private Common.FakturaGrunnlagFilter filter;
        private IList<Common.FakturaGrunnlag> fakturaGrunnlagListe;
        private ComboBox cbxOpprettetAar;
        private ComboBox cbxFilterKunde;

        public V_FakturaGrunnlagListe()
        {
            InitializeComponent();
            InitFilterToolstrip();

            p_FakturagrunnlagListe = new P_FakturaGrunnlagListe(this);
            filter = new FakturaGrunnlagFilter();
        }

        public int[] Aar
        {
            set
            {
                aar = value;
            }
        }

        public IList<Kunde> KundeListe
        {
            set
            {
                kundeListe = value;
            }
        }

        public FakturaGrunnlagFilter Filter
        {
            get
            {
                return filter;
            }
        }

        public IList<Common.FakturaGrunnlag> FakturaGrunnlagListe
        {
            set
            {
                fakturaGrunnlagListe = value;
            }
        }

        public ToolStrip ToolStrip
        {
            get
            {
                return tsFakturaGrunnlag;
            }
        }

        void IPrintable.Print()
        {
            throw new NotImplementedException();
        }

        protected void OnReportStatus(ReportStatusEventArgs e)
        {
            if (ReportStatus != null)
            {
                ReportStatus(this, e);
            }
        }

        private void InitFilterToolstrip()
        {
            Label lblOpprettetAar = new Label();
            lblOpprettetAar.Text = "Velg år:";
            lblOpprettetAar.TextAlign = ContentAlignment.MiddleCenter;
            lblOpprettetAar.Padding = new Padding(6);
            ToolStripControlHost _lblOpprettetAar = new ToolStripControlHost(lblOpprettetAar);

            cbxOpprettetAar = new ComboBox();
            cbxOpprettetAar.Width = 80;
            cbxOpprettetAar.DropDownStyle = ComboBoxStyle.DropDown;
            cbxOpprettetAar.Padding = new Padding(6);
            cbxOpprettetAar.DropDown += new EventHandler(cbxOpprettetAar_DropDown);
            ToolStripControlHost _cbxOpprettetAar = new ToolStripControlHost(cbxOpprettetAar);

            Label lblFilterKunde = new Label();
            lblFilterKunde.Text = "Velg kunde:";
            lblFilterKunde.TextAlign = ContentAlignment.MiddleCenter;
            lblFilterKunde.Padding = new Padding(6);
            ToolStripControlHost _lblFilterKunde = new ToolStripControlHost(lblFilterKunde);

            cbxFilterKunde = new ComboBox();
            cbxFilterKunde.Width = 150;
            cbxFilterKunde.DropDownStyle = ComboBoxStyle.DropDown;
            cbxFilterKunde.Padding = new Padding(6);
            cbxFilterKunde.TabStop = false;
            cbxFilterKunde.DropDown += new EventHandler(cbxFilterKunde_DropDown);
            ToolStripControlHost _cbxFilterKunde = new ToolStripControlHost(cbxFilterKunde);

            Label lblPrisoverslagStatus = new Label();
            lblPrisoverslagStatus.Text = "Velg status:";
            lblPrisoverslagStatus.TextAlign = ContentAlignment.MiddleCenter;
            lblPrisoverslagStatus.Padding = new Padding(6);
            ToolStripControlHost _lblPrisoverslagStatus = new ToolStripControlHost(lblPrisoverslagStatus);

            RadioButton rbPrisoverslagStatusUnderBehandling = new RadioButton();
            rbPrisoverslagStatusUnderBehandling.Text = "Under behandling";
            rbPrisoverslagStatusUnderBehandling.Checked = true;
            rbPrisoverslagStatusUnderBehandling.Padding = new Padding(6);
            ToolStripControlHost _rbPrisoverslagStatusUnderBehandling = new ToolStripControlHost(rbPrisoverslagStatusUnderBehandling);

            RadioButton rbPrisoverslagStatusSendt = new RadioButton();
            rbPrisoverslagStatusSendt.Text = "Sendt";
            rbPrisoverslagStatusSendt.Checked = false;
            rbPrisoverslagStatusSendt.Padding = new Padding(6);
            ToolStripControlHost _rbPrisoverslagStatusSendt = new ToolStripControlHost(rbPrisoverslagStatusSendt);

            RadioButton rbPrisoverslagStatusGodkjent = new RadioButton();
            rbPrisoverslagStatusGodkjent.Text = "Godkjent";
            rbPrisoverslagStatusGodkjent.Checked = false;
            rbPrisoverslagStatusGodkjent.Padding = new Padding(6);
            ToolStripControlHost _rbPrisoverslagStatusGodkjent = new ToolStripControlHost(rbPrisoverslagStatusGodkjent);

            tsFilter.Items.Add(_lblOpprettetAar);
            tsFilter.Items.Add(_cbxOpprettetAar);
            tsFilter.Items.Add(_lblFilterKunde);
            tsFilter.Items.Add(_cbxFilterKunde);
            tsFilter.Items.Add(_lblPrisoverslagStatus);
            tsFilter.Items.Add(_rbPrisoverslagStatusUnderBehandling);
            tsFilter.Items.Add(_rbPrisoverslagStatusSendt);
            tsFilter.Items.Add(_rbPrisoverslagStatusGodkjent);
        }

        void cbxFilterKunde_DropDown(object sender, EventArgs e)
        {
            if (kundeListe == null)
                p_FakturagrunnlagListe.HentKundeListe();

            cbxFilterKunde.BeginUpdate();
            cbxFilterKunde.Items.Clear();
            if ((kundeListe != null) && (kundeListe.Count > 0))
            {
                for (int nKunde = 0; nKunde < kundeListe.Count; nKunde++)
                {
                    cbxFilterKunde.Items.Add(kundeListe[nKunde].Navn);
                }
            }
            cbxFilterKunde.EndUpdate();
        }

        void cbxOpprettetAar_DropDown(object sender, EventArgs e)
        {
            if (aar == null)
                p_FakturagrunnlagListe.HentFakturaGrunnlagAar();

            cbxOpprettetAar.BeginUpdate();
            cbxOpprettetAar.Items.Clear();
            if ((aar != null) && (aar.Length > 0))
            {
                for (int nAar = 0; nAar < aar.Length; nAar++)
                {
                    cbxOpprettetAar.Items.Add(aar[nAar]);
                }
            }
            cbxOpprettetAar.EndUpdate();
        }

        private void hentFakturaGrunnlagListeTask_Run()
        {
            if (!hentFakturaGrunnlagListeTask.IsBusy)
            {
                OnReportStatus(new ReportStatusEventArgs("Henter liste med fakturagrunnlag..."));
                hentFakturaGrunnlagListeTask.RunWorkerAsync();
            }
        }

        private void hentFakturaGrunnlagListeTask_End()
        {
            OnReportStatus(new ReportStatusEventArgs("Ferdig"));
        }

        private void hentFakturaGrunnlagListeTask_Error()
        {
            OnReportStatus(new ReportStatusEventArgs("Feil ved henting av liste med fakturagrunnlag", true));
        }


        private void slettFakturaGrunnlagTask_Run(int fakturaGrunnlagId)
        {
            if (!slettFakturaGrunnlagTask.IsBusy)
            {
                OnReportStatus(new ReportStatusEventArgs("Sletter fakturagrunnlag..."));
                slettFakturaGrunnlagTask.RunWorkerAsync(fakturaGrunnlagId);
            }
        }

        private void slettFakturaGrunnlagTask_End()
        {
            OnReportStatus(new ReportStatusEventArgs("Ferdig"));
        }

        private void slettFakturaGrunnlagTask_Error()
        {
            OnReportStatus(new ReportStatusEventArgs("Feil ved sletting av fakturagrunnlag", true));
        }

        private void tsBtnFilter_CheckedChanged(object sender, EventArgs e)
        {
            tsFilter.Visible = tsBtnFilter.Checked;
        }

        private void hentFakturaGrunnlagListeTask_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            p_FakturagrunnlagListe.HentFakturaGrunnlagListe();
        }

        private void DrawListViewItems()
        {
            lwFakturaGrunnlag.BeginUpdate();
            lwFakturaGrunnlag.Items.Clear();
            if ((fakturaGrunnlagListe != null) && (fakturaGrunnlagListe.Count > 0))
            {
                Color underBehandlingBakgrunn = I_Konfig.UI.BehandlerBakgrunn;
                Color underBehandlingForgrunn = I_Konfig.UI.BehandlerForgrunn;
                Color sendtBakgrunn = I_Konfig.UI.SendtBakgrunn;
                Color sendtForgrunn = I_Konfig.UI.SendtForgrunn;

                for (int n = 0; n < fakturaGrunnlagListe.Count; n++)
                {
                    ListViewItem item = new ListViewItem();
                    item.Text = fakturaGrunnlagListe[n].Opprettet.ToShortDateString();
                    item.Tag = fakturaGrunnlagListe[n].Id;
                    item.SubItems.Add(fakturaGrunnlagListe[n].Kunde.Navn);
                    item.SubItems.Add(fakturaGrunnlagListe[n].Referanse);
                    item.SubItems.Add(fakturaGrunnlagListe[n].Beskrivelse);
                    item.SubItems.Add(Convert.ToString(fakturaGrunnlagListe[n].Beløp));
                    item.SubItems.Add(Convert.ToString(fakturaGrunnlagListe[n].Mva));
                    item.SubItems.Add(Convert.ToString(fakturaGrunnlagListe[n].Sum));
                    item.SubItems.Add(Convert.ToString(fakturaGrunnlagListe[n].Status));
                    switch (fakturaGrunnlagListe[n].Status)
                    {
                        case FakturaGrunnlagStatus.UnderBehandling:
                            item.BackColor = underBehandlingBakgrunn;
                            item.ForeColor = underBehandlingForgrunn;
                            break;
                        case FakturaGrunnlagStatus.Sendt:
                            item.BackColor = sendtBakgrunn;
                            item.ForeColor = sendtForgrunn;
                            break;
                    }
                    item.UseItemStyleForSubItems = false;
                    lwFakturaGrunnlag.Items.Add(item);
                }
            }
            lwFakturaGrunnlag.EndUpdate();
        }

        private void hentFakturaGrunnlagListeTask_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                DrawListViewItems();
                hentFakturaGrunnlagListeTask_End();
            }
            else
            {
                hentFakturaGrunnlagListeTask_Error();
            }
        }

        private void tsBtnNyttFakturaGrunnlag_Click(object sender, EventArgs e)
        {
            V_FakturaGrunnlag v_fakturaGrunnlag = new V_FakturaGrunnlag();
            DialogResult r = v_fakturaGrunnlag.ShowDialog();
            if (r == DialogResult.OK)
            {
                ((IV_Oppdaterbar)this).Oppdater(true);
            }
        }

        private void tsBtnRedigerFakturaGrunnlag_Click(object sender, EventArgs e)
        {
            int fakturaGrunnlagId = ((lwFakturaGrunnlag.SelectedItems != null) && (lwFakturaGrunnlag.SelectedItems.Count > 0)) ? 
                Convert.ToInt32(lwFakturaGrunnlag.SelectedItems[0].Tag) : -1;
            if (fakturaGrunnlagId != -1)
            {
                V_FakturaGrunnlag v_fakturaGrunnlag = new V_FakturaGrunnlag(fakturaGrunnlagId);
                DialogResult r = v_fakturaGrunnlag.ShowDialog();
                if (r == DialogResult.OK)
                {
                    ((IV_Oppdaterbar)this).Oppdater(true);
                }
            }
        }

        private void V_FakturaGrunnlagListe_Load(object sender, EventArgs e)
        {
            cbxOpprettetAar.Text = Convert.ToString(filter.Aar);
            cbxFilterKunde.Text = "Alle";

            hentFakturaGrunnlagListeTask_Run();
        }

        private void tsBtnSlettFakturaGrunnlag_Click(object sender, EventArgs e)
        {
            int fakturaGrunnlagId = ((lwFakturaGrunnlag.SelectedItems != null) && (lwFakturaGrunnlag.SelectedItems.Count > 0)) ? 
                Convert.ToInt32(lwFakturaGrunnlag.SelectedItems[0].Tag) : -1;
            if (fakturaGrunnlagId != -1)
            {
                slettFakturaGrunnlagTask_Run(fakturaGrunnlagId);
            }
        }

        private void slettFakturaGrunnlagTask_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            int fakturaGrunnlagId = Convert.ToInt32(e.Argument);
            p_FakturagrunnlagListe.SlettFakturaGrunnlag(fakturaGrunnlagId);
        }

        private void slettFakturaGrunnlagTask_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            ((IV_Oppdaterbar)this).Oppdater(true);
        }

        void IV_Oppdaterbar.Oppdater(bool bLast)
        {
            if (bLast)
                hentFakturaGrunnlagListeTask_Run();
            else
                DrawListViewItems();
        }
    }
}
