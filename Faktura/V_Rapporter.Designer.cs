﻿namespace Faktura
{
    partial class V_Rapporter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tsRapporter = new System.Windows.Forms.ToolStrip();
            this.wbRapport = new System.Windows.Forms.WebBrowser();
            this.initTask = new System.ComponentModel.BackgroundWorker();
            this.genererRapportTask = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // tsRapporter
            // 
            this.tsRapporter.Location = new System.Drawing.Point(0, 0);
            this.tsRapporter.Name = "tsRapporter";
            this.tsRapporter.Size = new System.Drawing.Size(651, 25);
            this.tsRapporter.TabIndex = 0;
            // 
            // wbRapport
            // 
            this.wbRapport.AllowWebBrowserDrop = false;
            this.wbRapport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbRapport.IsWebBrowserContextMenuEnabled = false;
            this.wbRapport.Location = new System.Drawing.Point(0, 25);
            this.wbRapport.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbRapport.Name = "wbRapport";
            this.wbRapport.Size = new System.Drawing.Size(651, 433);
            this.wbRapport.TabIndex = 1;
            // 
            // initTask
            // 
            this.initTask.DoWork += new System.ComponentModel.DoWorkEventHandler(this.initTask_DoWork);
            this.initTask.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.initTask_RunWorkerCompleted);
            // 
            // genererRapportTask
            // 
            this.genererRapportTask.DoWork += new System.ComponentModel.DoWorkEventHandler(this.genererRapportTask_DoWork);
            this.genererRapportTask.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.genererRapportTask_RunWorkerCompleted);
            // 
            // V_Rapporter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.wbRapport);
            this.Controls.Add(this.tsRapporter);
            this.DoubleBuffered = true;
            this.Name = "V_Rapporter";
            this.Size = new System.Drawing.Size(651, 458);
            this.Load += new System.EventHandler(this.V_Rapporter_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip tsRapporter;
        private System.Windows.Forms.WebBrowser wbRapport;
        private System.ComponentModel.BackgroundWorker initTask;
        private System.ComponentModel.BackgroundWorker genererRapportTask;
    }
}
