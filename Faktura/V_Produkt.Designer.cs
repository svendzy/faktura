﻿namespace Faktura
{
    partial class V_Produkt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtEnhetspris = new System.Windows.Forms.TextBox();
            this.lblEnhetspris = new System.Windows.Forms.Label();
            this.txtBeskrivelse = new System.Windows.Forms.TextBox();
            this.lblBeskrivelse = new System.Windows.Forms.Label();
            this.txtProduktkode = new System.Windows.Forms.TextBox();
            this.lblProduktkode = new System.Windows.Forms.Label();
            this.btnLagre = new System.Windows.Forms.Button();
            this.txtEnhet = new System.Windows.Forms.TextBox();
            this.lblEnhet = new System.Windows.Forms.Label();
            this.lblProduktId = new System.Windows.Forms.Label();
            this.txtProduktId = new System.Windows.Forms.Label();
            this.txtMvaSats = new System.Windows.Forms.TextBox();
            this.lblMvaSats = new System.Windows.Forms.Label();
            this.lagreBackgroundTask = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // txtEnhetspris
            // 
            this.txtEnhetspris.Location = new System.Drawing.Point(106, 132);
            this.txtEnhetspris.Name = "txtEnhetspris";
            this.txtEnhetspris.Size = new System.Drawing.Size(61, 20);
            this.txtEnhetspris.TabIndex = 3;
            // 
            // lblEnhetspris
            // 
            this.lblEnhetspris.AutoSize = true;
            this.lblEnhetspris.Location = new System.Drawing.Point(17, 135);
            this.lblEnhetspris.Name = "lblEnhetspris";
            this.lblEnhetspris.Size = new System.Drawing.Size(59, 13);
            this.lblEnhetspris.TabIndex = 0;
            this.lblEnhetspris.Text = "Enhetspris:";
            // 
            // txtBeskrivelse
            // 
            this.txtBeskrivelse.Location = new System.Drawing.Point(106, 78);
            this.txtBeskrivelse.MaxLength = 100;
            this.txtBeskrivelse.Name = "txtBeskrivelse";
            this.txtBeskrivelse.Size = new System.Drawing.Size(188, 20);
            this.txtBeskrivelse.TabIndex = 1;
            this.txtBeskrivelse.Tag = "";
            // 
            // lblBeskrivelse
            // 
            this.lblBeskrivelse.AutoSize = true;
            this.lblBeskrivelse.Location = new System.Drawing.Point(17, 82);
            this.lblBeskrivelse.Name = "lblBeskrivelse";
            this.lblBeskrivelse.Size = new System.Drawing.Size(64, 13);
            this.lblBeskrivelse.TabIndex = 0;
            this.lblBeskrivelse.Text = "Beskrivelse:";
            // 
            // txtProduktkode
            // 
            this.txtProduktkode.Location = new System.Drawing.Point(106, 52);
            this.txtProduktkode.MaxLength = 6;
            this.txtProduktkode.Name = "txtProduktkode";
            this.txtProduktkode.Size = new System.Drawing.Size(61, 20);
            this.txtProduktkode.TabIndex = 0;
            // 
            // lblProduktkode
            // 
            this.lblProduktkode.AutoSize = true;
            this.lblProduktkode.Location = new System.Drawing.Point(17, 55);
            this.lblProduktkode.Name = "lblProduktkode";
            this.lblProduktkode.Size = new System.Drawing.Size(71, 13);
            this.lblProduktkode.TabIndex = 0;
            this.lblProduktkode.Text = "Produktkode:";
            // 
            // btnLagre
            // 
            this.btnLagre.Location = new System.Drawing.Point(219, 194);
            this.btnLagre.Name = "btnLagre";
            this.btnLagre.Size = new System.Drawing.Size(75, 23);
            this.btnLagre.TabIndex = 5;
            this.btnLagre.Text = "Lagre";
            this.btnLagre.UseVisualStyleBackColor = true;
            this.btnLagre.Click += new System.EventHandler(this.btnLagre_Click);
            // 
            // txtEnhet
            // 
            this.txtEnhet.Location = new System.Drawing.Point(106, 104);
            this.txtEnhet.MaxLength = 10;
            this.txtEnhet.Name = "txtEnhet";
            this.txtEnhet.Size = new System.Drawing.Size(61, 20);
            this.txtEnhet.TabIndex = 2;
            // 
            // lblEnhet
            // 
            this.lblEnhet.AutoSize = true;
            this.lblEnhet.Location = new System.Drawing.Point(17, 107);
            this.lblEnhet.Name = "lblEnhet";
            this.lblEnhet.Size = new System.Drawing.Size(38, 13);
            this.lblEnhet.TabIndex = 0;
            this.lblEnhet.Text = "Enhet:";
            // 
            // lblProduktId
            // 
            this.lblProduktId.AutoSize = true;
            this.lblProduktId.Location = new System.Drawing.Point(17, 19);
            this.lblProduktId.Name = "lblProduktId";
            this.lblProduktId.Size = new System.Drawing.Size(59, 13);
            this.lblProduktId.TabIndex = 0;
            this.lblProduktId.Text = "Produkt-Id:";
            // 
            // txtProduktId
            // 
            this.txtProduktId.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtProduktId.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.txtProduktId.Location = new System.Drawing.Point(106, 19);
            this.txtProduktId.Name = "txtProduktId";
            this.txtProduktId.Size = new System.Drawing.Size(61, 20);
            this.txtProduktId.TabIndex = 0;
            this.txtProduktId.Text = "Automatisk";
            this.txtProduktId.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMvaSats
            // 
            this.txtMvaSats.Location = new System.Drawing.Point(106, 158);
            this.txtMvaSats.Name = "txtMvaSats";
            this.txtMvaSats.Size = new System.Drawing.Size(61, 20);
            this.txtMvaSats.TabIndex = 7;
            // 
            // lblMvaSats
            // 
            this.lblMvaSats.AutoSize = true;
            this.lblMvaSats.Location = new System.Drawing.Point(17, 161);
            this.lblMvaSats.Name = "lblMvaSats";
            this.lblMvaSats.Size = new System.Drawing.Size(53, 13);
            this.lblMvaSats.TabIndex = 6;
            this.lblMvaSats.Text = "Mva-sats:";
            // 
            // lagreBackgroundTask
            // 
            this.lagreBackgroundTask.DoWork += new System.ComponentModel.DoWorkEventHandler(this.lagreBackgroundTask_DoWork);
            this.lagreBackgroundTask.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.lagreBackgroundTask_RunWorkerCompleted);
            // 
            // V_Produkt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(313, 229);
            this.Controls.Add(this.txtMvaSats);
            this.Controls.Add(this.lblMvaSats);
            this.Controls.Add(this.txtProduktId);
            this.Controls.Add(this.lblProduktId);
            this.Controls.Add(this.txtEnhet);
            this.Controls.Add(this.lblEnhet);
            this.Controls.Add(this.btnLagre);
            this.Controls.Add(this.txtEnhetspris);
            this.Controls.Add(this.lblEnhetspris);
            this.Controls.Add(this.txtBeskrivelse);
            this.Controls.Add(this.lblBeskrivelse);
            this.Controls.Add(this.txtProduktkode);
            this.Controls.Add(this.lblProduktkode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "V_Produkt";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Nytt produkt";
            this.Load += new System.EventHandler(this.V_Produkt_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtEnhetspris;
        private System.Windows.Forms.Label lblEnhetspris;
        private System.Windows.Forms.TextBox txtBeskrivelse;
        private System.Windows.Forms.Label lblBeskrivelse;
        private System.Windows.Forms.TextBox txtProduktkode;
        private System.Windows.Forms.Label lblProduktkode;
        private System.Windows.Forms.Button btnLagre;
        private System.Windows.Forms.TextBox txtEnhet;
        private System.Windows.Forms.Label lblEnhet;
        private System.Windows.Forms.Label lblProduktId;
        private System.Windows.Forms.Label txtProduktId;
        private System.Windows.Forms.TextBox txtMvaSats;
        private System.Windows.Forms.Label lblMvaSats;
        private System.ComponentModel.BackgroundWorker lagreBackgroundTask;
    }
}