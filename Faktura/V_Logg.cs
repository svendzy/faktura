﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Faktura.Presentation;

namespace Faktura
{
    public partial class V_Logg : UserControl, IV_Logg, IV_ToolStrip, IPrintable, IReportStatus
    {
        private P_Logg p_Logg;
        private IList<Common.LoggElement> logg;
        public event ReportStatusEventHandler ReportStatus;

        public V_Logg()
        {
            InitializeComponent();

            p_Logg = new P_Logg(this);
            lwLogg.ColumnWeights = new float[] { 0.15f, 0.15f, 0.7f };
        }

        public ToolStrip ToolStrip
        {
            get
            {
                return tsLogg;
            }
        }

        public IList<Common.LoggElement> Logg
        {
            set
            {
                if (logg != null)
                    logg = null;
                logg = value;
            }
        }

        private void DrawListViewItems()
        {
            lwLogg.BeginUpdate();
            lwLogg.Items.Clear();
            for (int nElement = 0; nElement < logg.Count; nElement++)
            {
                ListViewItem element = new ListViewItem();
                element.Text = logg[nElement].Dato.ToShortDateString();
                element.SubItems.Add(logg[nElement].Dato.ToShortTimeString());
                element.SubItems.Add(logg[nElement].Beskrivelse);
                lwLogg.Items.Add(element);
            }
            lwLogg.EndUpdate();
        }

        private void V_Logg_Load(object sender, EventArgs e)
        {
            lwLogg.ReCalculateColumnWidths();
        }

        private void hentLoggBackgroundTask_DoWork(object sender, DoWorkEventArgs e)
        {
            p_Logg.HentLogg();
        }

        private void hentLoggBackgroundTask_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                DrawListViewItems();
                slettLoggTask_End();
            }
            else
            {
                slettLoggTask_Error();
            }
        }

        private void tsBtnSlettLogg_Click(object sender, EventArgs e)
        {
            slettLoggTask_Run();
        }

        private void slettLoggBackgroundTask_DoWork(object sender, DoWorkEventArgs e)
        {
            p_Logg.SlettLogg();
        }

        private void slettLoggBackgroundTask_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                this.Logg = new List<Common.LoggElement>();
                slettLoggTask_End();
            }
            else
            {
                slettLoggTask_Error();
            }
        }

        private void V_Logg_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                hentLoggTask_Run();
            }
        }

        private void hentLoggTask_Run()
        {
            if (!hentLoggBackgroundTask.IsBusy)
            {
                OnReportStatus(new ReportStatusEventArgs("Henter logg..."));
                hentLoggBackgroundTask.RunWorkerAsync();
            }
        }

        private void hentLoggTask_End()
        {
            OnReportStatus(new ReportStatusEventArgs("Ferdig"));
        }

        private void hentLoggTask_Error()
        {
            OnReportStatus(new ReportStatusEventArgs("Feil ved henting av logg"));
        }

        private void slettLoggTask_Run()
        {
            if (!slettLoggBackgroundTask.IsBusy)
            {
                OnReportStatus(new ReportStatusEventArgs("Sletter logg..."));
                slettLoggBackgroundTask.RunWorkerAsync();
            }
        }

        private void slettLoggTask_End()
        {
            OnReportStatus(new ReportStatusEventArgs("Ferdig"));
        }

        private void slettLoggTask_Error()
        {
            OnReportStatus(new ReportStatusEventArgs("Feil ved sletting av logg"));
        }

        void IPrintable.Print()
        {
            throw new NotImplementedException();
        }

        protected void OnReportStatus(ReportStatusEventArgs e)
        {
            if (ReportStatus != null)
            {
                ReportStatus(this, e);
            }
        }

        void IV_Oppdaterbar.Oppdater(bool bLast)
        {
            if (bLast)
                hentLoggTask_Run();
            else
                DrawListViewItems();
        }
    }
}
