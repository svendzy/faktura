﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faktura.Model
{
    public interface IModel
    {
        Common.Produkt HentProdukt(int produktId);
        void LagreProdukt(Common.Produkt produkt);
        IList<Common.FakturaPreview> HentFakturaListe(Common.FakturaFilter fakturaFilter);
        IList<Common.Kunde> HentKundeListe();
        IList<Common.Produkt> HentProduktListe();
        void LagreKunde(Common.Kunde kunde);
        Common.Faktura HentFaktura(int fakturaId, bool bHentFakturaLinjer = true);
        void LagreFaktura(Common.Faktura faktura, bool bLagreFakturaLinjer = true);
        void SlettProdukt(int produktId);
        void SendFaktura(int fakturaId);
        void KrediterFaktura(int fakturaId);
        void RegistrerInnbetaling(int fakturaId);
        void SlettFaktura(int fakturaId);
        int[] HentFakturaAar();
        IList<Common.KundeNavn> HentKundeNavn();
        string GenererFaktura(int fakturaId);
        IList<Common.LoggElement> HentLogg();
        void SlettLogg();
        string GenererRapport(int år);
        IList<Common.FakturaGrunnlag> HentFakturaGrunnlagListe(Common.FakturaGrunnlagFilter filter);
        void SendFakturaGrunnlag(int fakturaGrunnlagId);
        void GodkjennFakturaGrunnlag(int fakturaGrunnlagId);
        int[] HentFakturaGrunnlagAar();
        Common.FakturaGrunnlag HentFakturaGrunnlag(int fakturaGrunnlagId);
        void LagreFakturaGrunnlag(Common.FakturaGrunnlag fakturaGrunnlag);
        void SlettFakturaGrunnlag(int fakturaGrunnlagId);
    }
}
