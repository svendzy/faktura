﻿using System;

namespace Faktura.Model
{
    public class ModelException : System.ApplicationException
    {
        public ModelException() : base() { }
        public ModelException(string message) : base(message) { }
        public ModelException(string message, Exception exception) : base(message, exception) { }
    }
}
