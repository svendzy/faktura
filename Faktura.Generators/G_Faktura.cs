﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.IO;

using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

using Faktura.Config;

namespace Faktura.Generators
{
    public class G_Faktura : IGenerator
    {
        private Common.Faktura faktura;
        private string filnavn;
        private static XStringFormat sfLeft;
        private static XStringFormat sfCenter;
        private static XStringFormat sfRight;

        public G_Faktura(Common.Faktura faktura)
        {
            this.faktura = faktura;
            this.filnavn = Path.ChangeExtension(Path.GetTempFileName(), "pdf");
        }

        public string Filnavn
        {
            get
            {
                return filnavn;
            }
        }

        private static XStringFormat SFLeft
        {
            get
            {
                if (sfLeft != null)
                    return sfLeft;
                XStringFormat sf = new XStringFormat();
                sf.Alignment = XStringAlignment.Near;
                sf.LineAlignment = XLineAlignment.Center;
                sfLeft = sf;
                return sfLeft;
            }
        }

        private static XStringFormat SFCenter
        {
            get
            {
                if (sfCenter != null)
                    return sfCenter;
                XStringFormat sf = new XStringFormat();
                sf.Alignment = XStringAlignment.Center;
                sf.LineAlignment = XLineAlignment.Center;
                sfCenter = sf;
                return sfCenter;
            }
        }

        private static XStringFormat SFRight
        {
            get
            {
                if (sfRight != null)
                    return sfRight;
                XStringFormat sf = new XStringFormat();
                sf.Alignment = XStringAlignment.Far;
                sf.LineAlignment = XLineAlignment.Center;
                sfRight = sf;
                return sfRight;
            }
        }

        public void Generer()
        {
            using (PdfDocument document = new PdfDocument())
            {
                document.Info.Title = "Created with PDFsharp";
                PdfPage page = document.AddPage();
                page.Size = PageSize.A4;
                page.Orientation = PageOrientation.Portrait;
                using (XGraphics gfx = XGraphics.FromPdfPage(page))
                {
                    XFont smallFont = new XFont("Arial", 11, XFontStyle.Regular);
                    XFont normalFont = new XFont("Arial", 11, XFontStyle.Regular);
                    XFont largeFont = new XFont("Arial", 28, XFontStyle.Bold);

                    string sFakturaDato = (faktura.Dato != null) ? ((DateTime)faktura.Dato).ToShortDateString() : String.Empty;
                    string sForfallsDato = (faktura.ForfallsDato != null) ? ((DateTime)faktura.ForfallsDato).ToShortDateString() : String.Empty;

                    using (XImage imgGiro = XImage.FromGdiPlusImage(Resources.giro))
                    {
                        int iKalkulertBeløp = 0; // Convert.ToInt32(Math.Floor(faktura.KalkulertSum));
                        int rKalkulertBeløp = 0; // Convert.ToInt32((faktura.KalkulertSum - (double)iKalkulertBeløp) * 100);

                        gfx.DrawImage(imgGiro, new XRect(new XPoint(0, 0), new XSize(page.Width.Value, page.Height.Value)));

                        gfx.DrawString(I_Konfig.Foretak.KontoNr, smallFont, XBrushes.Black, 38, 570);
                        gfx.DrawString(String.Format("{0},{1}", iKalkulertBeløp, rKalkulertBeløp.ToString().PadLeft(2, '0')), smallFont, XBrushes.Black, new XRect(new XPoint(225, 555), new XSize(90, 25)), XStringFormats.Center);

                        gfx.DrawString(sForfallsDato, smallFont, XBrushes.Black, 470, 606);
                        gfx.DrawString(String.Format("Faktura-nr.: {0}", faktura.Nr), smallFont, XBrushes.Black, 40, 616);
                        gfx.DrawString("Vennligst oppgi fakturanummer når du betaler", smallFont, XBrushes.Black, 40, 630);

                        gfx.DrawString(faktura.Kunde.Navn, smallFont, XBrushes.Black, 60, 720);
                        gfx.DrawString(faktura.Kunde.Postadresse, smallFont, XBrushes.Black, 60, 732);
                        gfx.DrawString(String.Format("{0} {1}", faktura.Kunde.PostNr, faktura.Kunde.PostSted), smallFont, XBrushes.Black, 60, 744);

                        gfx.DrawString(I_Konfig.Foretak.Navn, smallFont, XBrushes.Black, 330, 720);
                        gfx.DrawString(I_Konfig.Foretak.Adresse1, smallFont, XBrushes.Black, 330, 732);
                        gfx.DrawString(String.Format("{0} {1}", 
                            I_Konfig.Foretak.PostNr, I_Konfig.Foretak.Poststed),
                            smallFont, XBrushes.Black, 330, 744);

                        gfx.DrawString(iKalkulertBeløp.ToString(), smallFont, XBrushes.Black, new XRect(new XPoint(220, 810), new XSize(60, 25)), SFRight);
                        gfx.DrawString(rKalkulertBeløp.ToString().PadLeft(2, '0'), smallFont, XBrushes.Black, new XRect(new XPoint(290, 810), new XSize(25, 25)), SFLeft);

                        gfx.DrawString(I_Konfig.Foretak.KontoNr, smallFont, XBrushes.Black, 360, 825);
                    }

                    gfx.DrawString("Faktura", largeFont, XBrushes.Black, 0, 36);

                    XFont font = new XFont("Arial", 12, XFontStyle.Regular);
                    gfx.DrawString("Kunde:", normalFont, XBrushes.Black, 0, 142);
                    gfx.DrawString(faktura.Kunde.Navn, normalFont, XBrushes.Black, 0, 156);
                    gfx.DrawString(faktura.Kunde.Postadresse, normalFont, XBrushes.Black, 0, 170);
                    gfx.DrawString(String.Format("{0} {1}", faktura.Kunde.PostNr, faktura.Kunde.PostSted), normalFont, XBrushes.Black, 0, 184);

                    gfx.DrawString(I_Konfig.Foretak.Navn, normalFont, XBrushes.Black, 350, 30);
                    gfx.DrawString(I_Konfig.Foretak.Adresse1, normalFont, XBrushes.Black, 350, 44);
                    gfx.DrawString(String.Format("{0} {1}", I_Konfig.Foretak.PostNr, I_Konfig.Foretak.Poststed),
                        normalFont, XBrushes.Black, 350, 58);

                    gfx.DrawString("Org-nr.:", normalFont, XBrushes.Black, 350, 86);
                    gfx.DrawString((I_Konfig.Foretak.MvaPliktig ? String.Concat(I_Konfig.Foretak.OrgNr, " MVA") :
                        I_Konfig.Foretak.OrgNr), normalFont, XBrushes.Black, 450, 86);
                    gfx.DrawString("Epost:", normalFont, XBrushes.Black, 350, 100);
                    gfx.DrawString(I_Konfig.Foretak.Epost, normalFont, XBrushes.Black, 450, 100);
                    gfx.DrawString("Nettsted:", normalFont, XBrushes.Black, 350, 114);
                    gfx.DrawString(I_Konfig.Foretak.Nettsted, normalFont, XBrushes.Black, 450, 114);
                    gfx.DrawString("Telefon:", normalFont, XBrushes.Black, 350, 128);
                    gfx.DrawString(I_Konfig.Foretak.Telefon, normalFont, XBrushes.Black, 450, 128);

                    gfx.DrawString("Faktura-nr.:", normalFont, XBrushes.Black, 350, 156);
                    gfx.DrawString(faktura.Nr.ToString(), normalFont, XBrushes.Black, 450, 156);
                    gfx.DrawString("Fakturadato:", normalFont, XBrushes.Black, 350, 170);
                    gfx.DrawString(sFakturaDato, normalFont, XBrushes.Black, 450, 170);
                    gfx.DrawString("Forfallsdato:", normalFont, XBrushes.Black, 350, 184);
                    gfx.DrawString(sForfallsDato, normalFont, XBrushes.Black, 450, 184);


                    XRect rectBeskrivelse = new XRect(new XPoint(0, 240), new XSize(290, 20));
                    XRect rectEnhetspris = new XRect(new XPoint(290, 240), new XSize(60, 20));
                    XRect rectAntall = new XRect(new XPoint(350, 240), new XSize(60, 20));
                    XRect rectMva = new XRect(new XPoint(410, 240), new XSize(80, 20));
                    XRect rectBeløp = new XRect(new XPoint(490, 240), new XSize(80, 20));

                    gfx.DrawString("Beskrivelse", normalFont, XBrushes.Black, rectBeskrivelse, SFLeft);
                    gfx.DrawString("Enhetspris", normalFont, XBrushes.Black, rectEnhetspris, SFRight);
                    gfx.DrawString("Antall", normalFont, XBrushes.Black, rectAntall, SFRight);
                    gfx.DrawString("Mva", normalFont, XBrushes.Black, rectMva, SFRight);
                    gfx.DrawString("Beløp", normalFont, XBrushes.Black, rectBeløp, SFRight);
                    gfx.DrawLine(XPens.Gray, new XPoint(0, 260), new XPoint(page.Width.Value, 260));

                    XRect rectBeskrivelse_0 = rectBeskrivelse;
                    rectBeskrivelse_0.Offset(0, 30);
                    XRect rectEnhetspris_0 = rectEnhetspris;
                    rectEnhetspris_0.Offset(0, 30);
                    XRect rectAntall_0 = rectAntall;
                    rectAntall_0.Offset(0, 30);
                    XRect rectMva_0 = rectMva;
                    rectMva_0.Offset(0, 30);
                    XRect rectBeløp_0 = rectBeløp;
                    rectBeløp_0.Offset(0, 30);

                    for (int nLinje = 0; nLinje < faktura.Linjer.Count; nLinje++)
                    {
                        gfx.DrawString(faktura.Linjer[nLinje].Beskrivelse, normalFont, XBrushes.Black, rectBeskrivelse_0, SFLeft);
                        gfx.DrawString(faktura.Linjer[nLinje].EnhetsPris.ToString("0.00"), normalFont, XBrushes.Black, rectEnhetspris_0, SFRight);
                        gfx.DrawString(faktura.Linjer[nLinje].Ant.ToString(), normalFont, XBrushes.Black, rectAntall_0, SFRight);
                        gfx.DrawString(faktura.Linjer[nLinje].KalkulertMva.ToString("0.00"), normalFont, XBrushes.Black, rectMva_0, SFRight);
                        gfx.DrawString(faktura.Linjer[nLinje].KalkulertSum.ToString("0.00"), normalFont, XBrushes.Black, rectBeløp_0, SFRight);
                        rectBeskrivelse_0.Offset(0, 14);
                        rectEnhetspris_0.Offset(0, 14);
                        rectAntall_0.Offset(0, 14);
                        rectMva_0.Offset(0, 14);
                        rectBeløp_0.Offset(0, 14);
                    }
                    gfx.DrawString(String.Format("Totalt å betale: {0:0.00}, herav mva. {1:0.00}", faktura.KalkulertSum, faktura.KalkulertMva),
                        normalFont, XBrushes.Black, new XRect(new XPoint(0, 500), new XSize(570, 20)), SFRight);

                    document.Save(filnavn);
                }
            }
        }
    }
}
