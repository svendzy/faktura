﻿using System;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_SlettLogg : DbCmd
    {
        public override void Execute()
        {
            DbHelper.ExecuteScalar("DP_LOGG", null);
        }
    }
}
