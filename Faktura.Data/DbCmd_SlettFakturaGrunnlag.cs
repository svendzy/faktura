﻿using System;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_SlettFakturaGrunnlag : DbCmd
    {
        private int fakturaGrunnlagId;

        public DbCmd_SlettFakturaGrunnlag(int fakturaGrunnlagId)
            : base()
        {
            this.fakturaGrunnlagId = fakturaGrunnlagId;
        }

        public override void Execute()
        {
            FbParameter[] param = new FbParameter[] 
            {
                new FbParameter("@ID", FbDbType.Integer)
            };
            param[0].Value = fakturaGrunnlagId;
            DbHelper.ExecuteScalar("DP_FAKTURAGRUNNLAG", param);
        }
    }
}
