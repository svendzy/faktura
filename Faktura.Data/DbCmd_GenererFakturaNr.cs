﻿using System;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_GenererFakturaNr : DbCmd
    {
        private int aar;

        public DbCmd_GenererFakturaNr(int aar)
            : base()
        {
            this.aar = aar;
        }

        public override void Execute()
        {
            FbParameter[] param = new FbParameter[] 
            {
                new FbParameter("@AAR", FbDbType.Integer)
            };
            param[0].Value = aar;
            base.Result = DbHelper.ExecuteScalar("SP_FAKTURA_NR", param, Connection, Transaction);
        }
    }
}
