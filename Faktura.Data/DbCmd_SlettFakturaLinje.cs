﻿using System;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_SlettFakturaLinje : DbCmd
    {
        private int fakturaLinjeId;

        public DbCmd_SlettFakturaLinje(int fakturaLinjeId)
            : base()
        {
            this.fakturaLinjeId = fakturaLinjeId;
        }

        public override void Execute()
        {
            FbParameter[] param = new FbParameter[] 
            {
                new FbParameter("@ID", FbDbType.Integer)
            };
            param[0].Value = fakturaLinjeId;
            DbHelper.ExecuteScalar("DP_FAKTURALINJE", param, Connection, Transaction);
        }
    }
}
