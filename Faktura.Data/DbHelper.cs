﻿using System;
using System.Drawing;
using System.IO;
using System.Configuration;
using System.Data;

using FirebirdSql.Data.FirebirdClient;
using Faktura.Config;

namespace Faktura.Data
{
    public static class DbHelper
    {
        private static string connectionString;

        static DbHelper()
        {
            connectionString = I_Konfig.Database.ConnectionString;
        }

        private static string BuildQueryString(string storedProcedure, FbParameter[] param)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("SELECT * FROM \"");
            sb.Append(storedProcedure);
            sb.Append("\"");
            if ((param != null) && (param.Length > 0))
            {
                sb.Append("(");
                for (int nParam = 0; nParam < param.Length; nParam++)
                {
                    sb.Append(param[nParam].ParameterName);
                    if (nParam < param.Length - 1)
                        sb.Append(",");
                }
                sb.Append(")");
            }
            sb.Append(";");
            return sb.ToString();
        }

        public static object ExecuteScalar(string storedProcedure, FbParameter[] param)
        {
            object retval = null;
            using (FbConnection conn = new FbConnection(connectionString))
            {
                conn.Open();
                using (FbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = BuildQueryString(storedProcedure, param);
                    cmd.CommandType = CommandType.Text;
                    if ((param != null) && (param.Length > 0))
                    {
                        for (int nParam = param.Length - 1; nParam >= 0; nParam--)
                            cmd.Parameters.Add(param[nParam]);
                    }
                    retval = cmd.ExecuteScalar();
                }
            }
            return retval;
        }

        public static object ExecuteScalar(string storedProcedure, FbParameter[] param, FbConnection connection, FbTransaction transaction)
        {
            object retval = null;
            using (FbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = BuildQueryString(storedProcedure, param);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = transaction;
                if ((param != null) && (param.Length > 0))
                {
                    for (int nParam = param.Length - 1; nParam >= 0; nParam--)
                        cmd.Parameters.Add(param[nParam]);
                }
                retval = cmd.ExecuteScalar();
            }
            return retval;
        }

        public static FbDataReader ExecuteDataReader(string storedProcedure, FbParameter[] param)
        {
            FbConnection conn = null;
            FbCommand cmd = null;
            FbDataReader reader = null;
            try
            {
                conn = new FbConnection(connectionString);
                conn.Open();
                cmd = conn.CreateCommand();
                cmd.CommandText = BuildQueryString(storedProcedure, param);
                cmd.CommandType = CommandType.Text;
                if ((param != null) && (param.Length > 0))
                {
                    for (int nParam = param.Length - 1; nParam >= 0; nParam--)
                        cmd.Parameters.Add(param[nParam]);
                }
                reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                if (cmd != null)
                    cmd.Dispose();
                if ((conn != null) &&
                    (conn.State != ConnectionState.Closed))
                    conn.Dispose();
                throw ex;
            }
            return reader;
        }

        public static byte[] ImageToByteArray(System.Drawing.Image image)
        {
            MemoryStream ms = new MemoryStream();
            image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms.ToArray();
        }

        public static Image ByteArrayToImage(byte[] byteArray)
        {
            MemoryStream ms = new MemoryStream(byteArray);
            Image image = Image.FromStream(ms);
            return image;
        }

        public static int[] GetOrdinals(string[] columns, FbDataReader reader)
        {
            int[] intArray = new int[columns.Length];
            for (int nColumn = 0; nColumn < columns.Length; nColumn++)
            {
                intArray[nColumn] = reader.GetOrdinal(columns[nColumn]);
            }
            return intArray;
        }

        public static FbConnection CreateConnection()
        {
            return new FbConnection(connectionString);
        }
    }
}
