﻿using System;
using System.Collections.Generic;
using System.Text;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_HentFakturaAar : DbCmd
    {
        public DbCmd_HentFakturaAar()
            : base()
        {
        }

        public override void Execute()
        {
            List<int> fakturaAar = new List<int>();
            FbDataReader reader = null;
            try
            {
                reader = DbHelper.ExecuteDataReader("SP_FAKTURA_AAR", null);
                if (reader.HasRows)
                {
                    while (reader.Read())
                        fakturaAar.Add(Convert.ToInt32(reader["AAR"]));
                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            base.Result = fakturaAar.ToArray();
        }
    }
}
