﻿using System;
using System.Collections.Generic;
using System.Text;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_HentFakturaGrunnlag : DbCmd
    {
        private int fakturaGrunnlagId;
        private bool bHentFakturaLinjer;

        public DbCmd_HentFakturaGrunnlag(int fakturaGrunnlagId, bool bHentFakturaLinjer = true)
            : base()
        {
            this.fakturaGrunnlagId = fakturaGrunnlagId;
            this.bHentFakturaLinjer = bHentFakturaLinjer; 
        }

        public override void Execute()
        {
            Common.FakturaGrunnlag fakturaGrunnlag = null;

            FbDataReader reader = null;
            try
            {
                FbParameter[] param = new FbParameter[] { new FbParameter("@ID", FbDbType.Integer) };
                param[0].Value = fakturaGrunnlagId;
                reader = DbHelper.ExecuteDataReader("SP_FAKTURAGRUNNLAG", param);
                if (reader.Read())
                {
                    fakturaGrunnlag = new Common.FakturaGrunnlag();
                    fakturaGrunnlag.Id = Convert.ToInt32(reader["FG_ID"]);
                    fakturaGrunnlag.Opprettet = Convert.ToDateTime(reader["FG_OPPRETTET"]);
                    fakturaGrunnlag.Referanse = Convert.ToString(reader["FG_REFERANSE"]);
                    fakturaGrunnlag.Beskrivelse = Convert.ToString(reader["FG_BESKRIVELSE"]);
                    fakturaGrunnlag.Beløp = Convert.ToDouble(reader["FG_BELOP"]);
                    fakturaGrunnlag.Mva = Convert.ToDouble(reader["FG_MVA"]);
                    fakturaGrunnlag.Sum = Convert.ToDouble(reader["FG_SUM"]);
                    fakturaGrunnlag.Status = (Common.FakturaGrunnlagStatus)Convert.ToInt32(reader["FG_STATUS"]);

                    int kundeId = Convert.ToInt32(reader["FG_KU_ID"]);
                    DbCmd_HentKunde cmd_HentKunde = new DbCmd_HentKunde(kundeId);
                    cmd_HentKunde.Execute();
                    fakturaGrunnlag.Kunde = (Common.Kunde)cmd_HentKunde.Result;

                    if (bHentFakturaLinjer)
                    {
                        DbCmd_HentFakturaLinjer cmd_HentFakturaLinjer = new DbCmd_HentFakturaLinjer(fakturaGrunnlagId);
                        cmd_HentFakturaLinjer.Execute();
                        fakturaGrunnlag.Linjer = (List<Common.FakturaLinje>)cmd_HentFakturaLinjer.Result;
                    }
                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            base.Result = fakturaGrunnlag;
        }
    }
}
