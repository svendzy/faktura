﻿using System;
using System.Collections.Generic;
using System.Text;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_HentKundeNavn : DbCmd
    {
        public DbCmd_HentKundeNavn() : base()
        {
        }

        public override void Execute()
        {
            List<Common.KundeNavn> kundeNavnListe = new List<Common.KundeNavn>();

            FbDataReader reader = null;
            try
            {
                reader = DbHelper.ExecuteDataReader("SP_KUNDELISTE_NAVN", null);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Common.KundeNavn kundeNavn = new Common.KundeNavn();
                        kundeNavn.Id = Convert.ToInt32(reader["KU_ID"]);
                        kundeNavn.Navn = Convert.ToString(reader["KU_NAVN"]);
                        kundeNavnListe.Add(kundeNavn);
                    }
                }
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            base.Result = kundeNavnListe;
        }
    }
}
