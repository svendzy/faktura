﻿using System;

using FirebirdSql.Data.FirebirdClient;

namespace Faktura.Data
{
    public class DbCmd_HentRapport : DbCmd
    {
        private int år;

        public DbCmd_HentRapport(int år)
            : base()
        {
            this.år = år;
        }

        public override void Execute()
        {
            int nMåned = 0, nTermin = 0;
            FbDataReader reader = null;

            Common.Rapport rapport = new Common.Rapport();

            try
            {
                rapport.År = år;

                FbParameter[] param = new FbParameter[] { new FbParameter("@AAR", FbDbType.Integer) };
                param[0].Value = år;

                reader = DbHelper.ExecuteDataReader("SP_RAPPORT_AAR", param);
                while (reader.Read())
                {
                    rapport.BruttoBeløp = Convert.ToDouble(reader["FA_BELOP_INNBETALT"]);
                    rapport.Mva = Convert.ToDouble(reader["FA_BELOP_MVA"]);
                }
                reader.Dispose();

                reader = DbHelper.ExecuteDataReader("SP_RAPPORT_MAANED", param);
                while (reader.Read())
                {
                    Common.RapportElement rapportElement = new Common.RapportElement();
                    rapportElement.BruttoBeløp = Convert.ToDouble(reader["FA_BELOP_INNBETALT"]);
                    rapportElement.Mva = Convert.ToDouble(reader["FA_BELOP_MVA"]);
                    rapport.MånedsRapport[nMåned++] = rapportElement;
                }
                reader.Dispose();

                reader = DbHelper.ExecuteDataReader("SP_RAPPORT_TERMIN", param);
                while (reader.Read())
                {
                    Common.RapportElement rapportElement = new Common.RapportElement();
                    rapportElement.BruttoBeløp = Convert.ToDouble(reader["FA_BELOP_INNBETALT"]);
                    rapportElement.Mva = Convert.ToDouble(reader["FA_BELOP_MVA"]);
                    rapport.TerminRapport[nTermin++] = rapportElement;
                }
                Result = rapport;
            }
            finally
            {
                if (reader != null) reader.Dispose();
            }
        }
    }
}
