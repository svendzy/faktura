﻿using System;
using System.Collections.Generic;

using Faktura.Common;

namespace Faktura.Presentation
{
    public interface IV_Rapport : IView
    {
        int[] RapportÅr { set; }
        string RapportFil { set; }
    }
}
