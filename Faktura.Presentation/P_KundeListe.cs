﻿using System;
using System.Collections.Generic;
using System.Text;

using Faktura.Model;

namespace Faktura.Presentation
{
    public class P_KundeListe : Presenter<IV_KundeListe>
    {
        public P_KundeListe(IV_KundeListe view)
            : base(view)
        {
        }

        public void HentKundeListe()
        {
            View.KundeListe = Model.HentKundeListe();
        }
    }
}
