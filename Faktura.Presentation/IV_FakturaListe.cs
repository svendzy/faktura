﻿using System;
using System.Collections.Generic;

using Faktura.Common;

namespace Faktura.Presentation
{
    public interface IV_FakturaListe : IView, IV_Oppdaterbar
    {
        IList<Common.KundeNavn> KundeNavn { set; }
        IList<Common.FakturaPreview> FakturaListe { set; }
        Common.FakturaFilter FakturaFilter { get; set; }
        int ValgtFakturaId { get; }
        int[] FakturaAar { get; set; }
    }
}
