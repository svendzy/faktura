﻿using System;
using System.Collections.Generic;

using Faktura.Common;

namespace Faktura.Presentation
{
    public interface IV_Kunde : IView
    {
        Kunde Kunde { get; set; }
    }
}
