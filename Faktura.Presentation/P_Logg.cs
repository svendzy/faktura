﻿using System;
using System.Collections.Generic;
using System.Text;

using Faktura.Model;
using Faktura.Common;

namespace Faktura.Presentation
{
    public class P_Logg : Presenter<IV_Logg>
    {
        public P_Logg(IV_Logg view)
            : base(view)
        {
        }

        public void HentLogg()
        {
            View.Logg = Model.HentLogg();
        }

        public void SlettLogg()
        {
            Model.SlettLogg();
        }
    }
}
