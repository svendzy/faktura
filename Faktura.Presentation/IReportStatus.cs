﻿using System;

namespace Faktura.Presentation
{
    public delegate void ReportStatusEventHandler(object sender, ReportStatusEventArgs e);

    public interface IReportStatus
    {
        event ReportStatusEventHandler ReportStatus;
    }
}
