﻿using System;
using System.Collections.Generic;

using Faktura.Common;

namespace Faktura.Presentation
{
    public interface IV_KundeVelger : IView
    {
        IList<Common.Kunde> KundeListe { set; }
        Common.Kunde Kunde { get; set; }
    }
}
