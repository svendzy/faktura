﻿using System;
using System.Collections.Generic;
using System.Text;

using Faktura.Model;
using Faktura.Common;

namespace Faktura.Presentation
{
    public class P_Kunde : Presenter<IV_Kunde>
    {
        public P_Kunde(IV_Kunde view) : base(view)
        {
        }

        public void Init()
        {
            View.Kunde = new Kunde();
        }

        public void LagreKunde()
        {
            Model.LagreKunde(View.Kunde);
        }
    }
}
