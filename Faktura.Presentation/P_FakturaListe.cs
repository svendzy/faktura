﻿using System;
using System.Collections.Generic;
using System.Text;

using Faktura.Common;

namespace Faktura.Presentation
{
    public class P_FakturaListe : Presenter<IV_FakturaListe>
    {
        public P_FakturaListe(IV_FakturaListe view)
            : base(view)
        {
        }

        public void Init()
        {
            FakturaFilter fakturaFilter = new FakturaFilter();
            View.FakturaFilter = fakturaFilter;
        }

        public void HentFakturaListe()
        {
            FakturaFilter fakturaFilter = View.FakturaFilter;
            View.FakturaListe = Model.HentFakturaListe(fakturaFilter);
        }

        public void HentFakturaAar()
        {
            View.FakturaAar = Model.HentFakturaAar();
        }

        public void SendFaktura()
        {
            Model.SendFaktura(View.ValgtFakturaId);
        }

        public void KrediterFaktura()
        {
            Model.KrediterFaktura(View.ValgtFakturaId); 
        }

        public void RegistrerInnbetaling()
        {
            Model.RegistrerInnbetaling(View.ValgtFakturaId);
        }

        public void SlettFaktura()
        {

            Model.SlettFaktura(View.ValgtFakturaId);
        }

        public void HentKundeNavn()
        {
            View.KundeNavn = Model.HentKundeNavn();
        }

        public void SendPrisoverslag()
        {
           // Model.SendPrisoverslag(View.ValgtFakturaId);
        }

        public void VisFaktura()
        {
            string pdfFil = Model.GenererFaktura(View.ValgtFakturaId);
            System.Diagnostics.Process.Start(pdfFil);
        }
    }
}
