﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faktura.Common
{
    public class LoggElement
    {
        private DateTime dato;
        private string beskrivelse;

        public LoggElement()
        {
            dato = DateTime.Now;
            beskrivelse = String.Empty;
        }

        public DateTime Dato
        {
            get 
            { 
                return dato;
            }
            set 
            { 
                dato = value;
            }
        }

        public string Beskrivelse
        {
            get 
            { 
                return beskrivelse; 
            }
            set 
            { 
                beskrivelse = value; 
            }
        }
    }
}
