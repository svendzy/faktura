﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faktura.Common
{
    public class KundeNavn
    {
        private int id;
        private string navn;

        public KundeNavn()
        {
            this.id = -1;
            this.navn = String.Empty;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Navn
        {
            get { return navn; }
            set { navn = value; }
        }
    }
}
