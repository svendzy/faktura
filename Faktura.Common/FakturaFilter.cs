﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faktura.Common
{
    public enum FakturaFilterStatus
    {
        Behandler,
        Sendt,
        Innbetalt,
        Alle
    }

    public class FakturaFilter
    {
        private int aar;
        private FakturaFilterStatus status;
        private int kundeId;

        public FakturaFilter()
        {
            this.aar = DateTime.Now.Year;
            this.status = FakturaFilterStatus.Alle;
            this.kundeId = -1;
        }

        public int Aar
        {
            get { return aar; }
            set { aar = value; }
        }

        public FakturaFilterStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        public int KundeId
        {
            get { return kundeId; }
            set { kundeId = value; }
        }
    }
}
