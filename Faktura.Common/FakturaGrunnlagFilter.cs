﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faktura.Common
{
    public class FakturaGrunnlagFilter
    {
        private int aar;
        private FakturaGrunnlagStatus status;
        private int kundeId;

        public FakturaGrunnlagFilter()
        {
            this.aar = DateTime.Now.Year;
            this.status = FakturaGrunnlagStatus.UnderBehandling;
            this.kundeId = -1;
        }

        public int Aar
        {
            get { return aar; }
            set { aar = value; }
        }

        public FakturaGrunnlagStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        public int KundeId
        {
            get { return kundeId; }
            set { kundeId = value; }
        }
    }
}
