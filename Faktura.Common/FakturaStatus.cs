﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faktura.Common
{
    public enum FakturaStatus
    {
        KlarForSending,
        Sendt,
        Purring1,
        Purring2,
        Inkasso,
        Kreditert,
        Innbetalt
    }
}
