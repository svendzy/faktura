﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faktura.Common
{
    public enum FakturaGrunnlagStatus
    {
        UnderBehandling,
        Sendt,
        Godkjent
    }
}
