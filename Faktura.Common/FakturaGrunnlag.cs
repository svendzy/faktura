﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faktura.Common
{
    public class FakturaGrunnlag
    {
        private int id;
        private DateTime opprettet;
        private string referanse;
        private string beskrivelse;
        private double beløp;
        private double mva;
        private double sum;
        private FakturaGrunnlagStatus status;
        private Kunde kunde;
        private IList<FakturaLinje> fakturaLinjer;

        public FakturaGrunnlag()
        {
            this.id = -1;
            this.opprettet = DateTime.Now;
            this.referanse = String.Empty;
            this.beskrivelse = String.Empty;
            this.beløp = 0;
            this.mva = 0;
            this.sum = 0;
            this.status = FakturaGrunnlagStatus.UnderBehandling;
            this.kunde = null;
            this.fakturaLinjer = new List<FakturaLinje>();
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public DateTime Opprettet
        {
            get { return opprettet; }
            set { opprettet = value; }
        }

        public string Referanse
        {
            get { return referanse; }
            set { referanse = value; }
        }

        public string Beskrivelse
        {
            get { return beskrivelse; }
            set { beskrivelse = value; }
        }

        public double Beløp
        {
            get { return beløp; }
            set { beløp = value; }
        }

        public double Mva
        {
            get { return mva; }
            set { mva = value; }
        }

        public double Sum
        {
            get { return sum; }
            set { sum = value; }
        }

        public FakturaGrunnlagStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        public IList<FakturaLinje> Linjer
        {
            get { return fakturaLinjer; }
            set { fakturaLinjer = value; }
        }

        public Kunde Kunde
        {
            get { return kunde; }
            set { kunde = value; }
        }
    }
}
