﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faktura.Config
{
    public static class I_Konfig
    {
        public static I_Foretak Foretak
        {
            get
            {
                return new I_Foretak();
            }
        }

        public static I_UI UI
        {
            get
            {
                return new I_UI();
            }
        }

        public static I_Database Database
        {
            get
            {
                return new I_Database();
            }
        }

        public static void Lagre()
        {
            Config.Instance().Save();
        }
    }
}
