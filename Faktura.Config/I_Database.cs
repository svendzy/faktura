﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faktura.Config
{
    public class I_Database
    {
        private string connectionString;

        public I_Database()
        {
            connectionString = Config.Instance().ConnectionStrings["Db"];
        }

        public string ConnectionString
        {
            get
            {
                return connectionString;
            }
        }
    }
}
