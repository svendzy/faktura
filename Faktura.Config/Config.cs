﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Faktura.Config
{
    internal class Config
    {
        private static Config _instance = null;
        
        private Configuration _config;
        private AppSettings _appSettings;
        private ConnectionStrings _connectionStrings;

        private static object syncObject = new object();

        public static Config Instance()
        {
            lock (syncObject)
            {
                if (_instance == null)
                {
                    _instance = new Config(
                        ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
                        );
                }
            }
            return _instance;
        }

        private Config(Configuration config)
        {
            _config = config;
            _appSettings = new AppSettings(config);
            _connectionStrings = new ConnectionStrings(config);
        }

        public ConnectionStrings ConnectionStrings
        {
            get
            {
                return _connectionStrings;
            }
        }

        public AppSettings AppSettings
        {
            get
            {
                return _appSettings;
            }
        }

        public void Save()
        {
            _config.Save(ConfigurationSaveMode.Full);
            _instance = null;
        }
    }
}
